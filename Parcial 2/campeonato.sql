-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-11-2021 a las 17:02:19
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `campeonato`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arbitro`
--

CREATE TABLE `arbitro` (
  `idArbitro` tinyint(3) UNSIGNED NOT NULL,
  `nombreArbitro` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidoArbitro` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `autorizado` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `arbitro`
--

INSERT INTO `arbitro` (`idArbitro`, `nombreArbitro`, `apellidoArbitro`, `autorizado`) VALUES
(1, 'Néstor', 'Pitana', 1),
(2, 'Patricio', 'Loustau', 1),
(3, 'Fernando', 'Rapallini', 1),
(4, 'Juan Pablo', 'Bellati', 0),
(5, 'Mauro', 'Vigliano', 0),
(6, 'Darío', 'Herrera', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `idEquipo` tinyint(3) UNSIGNED NOT NULL,
  `nombreEquipo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `registrado` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `idTecnicoN` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`idEquipo`, `nombreEquipo`, `registrado`, `idTecnicoN`) VALUES
(1, 'Paris Saint-Germain', 1, 1),
(2, 'Niza', 1, 2),
(3, 'Lens', 1, 3),
(4, 'Olympique De Marsella', 1, 4),
(5, 'Rennes', 1, 5),
(6, 'Angers', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gol`
--

CREATE TABLE `gol` (
  `idGol` int(11) UNSIGNED NOT NULL,
  `minutoGol` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `idJugadorN` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `idPartidoN` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gol`
--

INSERT INTO `gol` (`idGol`, `minutoGol`, `idJugadorN`, `idPartidoN`) VALUES
(1, 10, 8, 1),
(2, 25, 11, 1),
(3, 30, 16, 2),
(4, 70, 16, 2),
(5, 10, 20, 3),
(6, 15, 21, 3),
(7, 40, 4, 3),
(8, 55, 4, 3),
(9, 80, 5, 3),
(10, 22, 4, 4),
(11, 25, 5, 4),
(12, 60, 4, 4),
(13, 30, 18, 5),
(14, 90, 26, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador`
--

CREATE TABLE `jugador` (
  `idJugador` smallint(5) UNSIGNED NOT NULL,
  `nombreJugador` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidoJugador` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `posicionJugador` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `contratado` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `idEquipoN` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `jugador`
--

INSERT INTO `jugador` (`idJugador`, `nombreJugador`, `apellidoJugador`, `posicionJugador`, `contratado`, `idEquipoN`) VALUES
(1, 'Gianluigi ', 'Donnarumma', 'Arquero', 1, 1),
(2, 'Achraf ', 'Hakimi', 'Defensor', 1, 1),
(3, 'Marcos (Marquinhos)', 'Aoás Corrêa', 'Defensor', 1, 1),
(4, 'Lionel ', 'Messi', 'Mediocampista', 1, 1),
(5, 'Kylian ', 'Mbappé', 'Delantero', 1, 1),
(7, 'Thomas ', 'Vincensini', 'Arquero', 1, 2),
(8, 'Kevin ', 'Danso', 'Defensor', 1, 2),
(9, 'Christopher ', 'Wooh', 'Defensor', 1, 2),
(10, 'Deiver ', 'Machado', 'Mediocampista', 1, 2),
(11, 'Jules ', 'Keita', 'Delantero', 1, 2),
(12, 'Marcin ', 'Bulka', 'Arquero', 1, 3),
(13, 'Racine ', 'Coly', 'Defensor', 1, 3),
(14, 'Hicham ', 'Mahou', 'Defensor', 1, 3),
(15, 'Morgan ', 'Schneiderlin', 'Mediocampista', 1, 3),
(16, 'Andy ', 'Delort', 'Delantero', 1, 3),
(17, 'Manuel ', 'Nazaretian', 'Arquero', 1, 4),
(18, 'Amay ', 'Caprice', 'Defensor', 1, 4),
(19, 'Álvaro ', 'González', 'Defensor', 1, 4),
(20, 'Maxime ', 'Lopez', 'Mediocampista', 1, 4),
(21, 'Arkadiusz ', 'Milik', 'Delantero', 1, 4),
(22, 'Pépé ', 'Bonet', 'Arquero', 1, 5),
(23, 'Lilian ', 'Brassier', 'Defensor', 1, 5),
(24, 'Adrien ', 'Truffert', 'Defensor', 1, 5),
(25, 'Andy ', 'Diouf', 'Mediocampista', 1, 5),
(26, 'Martin ', 'Terrier', 'Delantero', 1, 5),
(27, 'Anthony ', 'Mandrea', 'Arquero', 1, 6),
(28, 'Abdoulaye ', 'Bamba', 'Defensor', 1, 6),
(29, 'Kevin ', 'Boma', 'Defensor', 1, 6),
(30, 'Thomas ', 'Mangani', 'Mediocampista', 1, 6),
(31, 'Sofiane ', 'Boufal', 'Delantero', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medioaudiovisual`
--

CREATE TABLE `medioaudiovisual` (
  `idMedioAudiovisual` smallint(5) UNSIGNED NOT NULL,
  `nombreMedioAudiovisual` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `autorizado` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `codigoTipoDeMedioN` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medioaudiovisual`
--

INSERT INTO `medioaudiovisual` (`idMedioAudiovisual`, `nombreMedioAudiovisual`, `autorizado`, `codigoTipoDeMedioN`) VALUES
(1, 'TyC', 1, 1),
(2, 'ESPN', 1, 1),
(3, 'ESPN2', 1, 1),
(4, 'TNT SPORTS', 1, 1),
(5, 'FOX SPORTS', 1, 1),
(6, 'FOX SPORTS 2', 1, 1),
(7, 'La Red AM', 1, 2),
(8, 'ESPN Radio', 1, 2),
(9, 'FOX Radio', 1, 2),
(10, 'Continental', 1, 2),
(11, 'La redonda', 1, 2),
(12, 'Ibai Llanos', 1, 3),
(13, 'Los displicentes', 1, 3),
(14, 'Web sports', 1, 3),
(15, 'Pelota online', 1, 3),
(16, 'Futbol libre', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

CREATE TABLE `partido` (
  `idPartido` smallint(5) UNSIGNED NOT NULL,
  `horarioPartido` datetime DEFAULT NULL,
  `partidoSinJugar` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `idEquipoLocal` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `idEquipoVisitante` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `idArbitroN` tinyint(3) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `partido`
--

INSERT INTO `partido` (`idPartido`, `horarioPartido`, `partidoSinJugar`, `idEquipoLocal`, `idEquipoVisitante`, `idArbitroN`) VALUES
(1, '2021-10-02 11:00:00', 0, 2, 5, 1),
(2, '2021-10-02 14:00:00', 0, 3, 6, 2),
(3, '2021-10-02 17:00:00', 0, 4, 1, 3),
(4, '2021-10-09 11:00:00', 0, 1, 3, 1),
(5, '2021-10-09 14:00:00', 0, 5, 4, 2),
(6, '2021-10-09 17:00:00', 0, 6, 2, 3),
(7, '2021-10-16 11:00:00', 0, 2, 1, 1),
(8, '2021-10-16 14:00:00', 0, 3, 4, 2),
(9, '2021-10-16 17:00:00', 0, 6, 5, 3),
(10, '2021-10-23 11:00:00', 0, 1, 6, 1),
(11, '2021-10-23 14:00:00', 1, 3, 5, 2),
(12, '2021-10-23 17:00:00', 1, 4, 2, 3),
(13, '2021-10-30 11:00:00', 1, 2, 3, 1),
(14, '2021-10-30 14:00:00', 1, 5, 1, 2),
(15, '2021-10-30 17:00:00', 1, 6, 4, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tecnico`
--

CREATE TABLE `tecnico` (
  `idTecnico` tinyint(3) UNSIGNED NOT NULL,
  `nombreTecnico` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidoTecnico` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `contratado` tinyint(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tecnico`
--

INSERT INTO `tecnico` (`idTecnico`, `nombreTecnico`, `apellidoTecnico`, `contratado`) VALUES
(1, ' Mauricio ', 'Pochettino', 1),
(2, 'Franck ', 'Haise', 1),
(3, 'Christophe ', 'Galtier', 1),
(4, 'Jorge Luis ', 'Sampaoli Moya', 1),
(5, 'Bruno ', 'Génésio', 1),
(6, 'Gérald ', 'Baticle', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodemedio`
--

CREATE TABLE `tipodemedio` (
  `codigoTipoDeMedio` smallint(5) UNSIGNED NOT NULL,
  `nombreTipoDeMedio` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipodemedio`
--

INSERT INTO `tipodemedio` (`codigoTipoDeMedio`, `nombreTipoDeMedio`) VALUES
(1, 'Television'),
(2, 'Radio'),
(3, 'Streaming');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transmision`
--

CREATE TABLE `transmision` (
  `idMedioAudiovisualN` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `idPartidoM` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `transmision`
--

INSERT INTO `transmision` (`idMedioAudiovisualN`, `idPartidoM`) VALUES
(1, 1),
(1, 3),
(1, 5),
(1, 7),
(1, 9),
(1, 11),
(1, 13),
(1, 15),
(7, 1),
(7, 3),
(7, 5),
(7, 7),
(7, 9),
(7, 11),
(7, 13),
(7, 15),
(2, 2),
(2, 4),
(2, 6),
(2, 8),
(2, 10),
(2, 12),
(2, 14),
(8, 2),
(8, 4),
(8, 6),
(8, 8),
(8, 10),
(8, 12),
(8, 14),
(12, 1),
(12, 4),
(12, 7),
(12, 10),
(12, 13),
(13, 2),
(13, 5),
(13, 8),
(13, 11),
(13, 14),
(14, 3),
(14, 6),
(14, 9),
(14, 12),
(14, 15),
(15, 1),
(15, 5),
(15, 9),
(16, 7),
(16, 11),
(16, 15),
(5, 4),
(5, 8),
(5, 12),
(6, 6),
(6, 8),
(6, 10),
(9, 1),
(9, 2),
(9, 5),
(9, 6),
(9, 8),
(9, 9),
(9, 12),
(10, 3),
(10, 4),
(10, 6),
(10, 7),
(10, 8),
(10, 9),
(10, 14),
(10, 15);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `arbitro`
--
ALTER TABLE `arbitro`
  ADD PRIMARY KEY (`idArbitro`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`idEquipo`),
  ADD KEY `idTecnico` (`idTecnicoN`);

--
-- Indices de la tabla `gol`
--
ALTER TABLE `gol`
  ADD PRIMARY KEY (`idGol`),
  ADD KEY `idJugador` (`idJugadorN`),
  ADD KEY `idPartido` (`idPartidoN`);

--
-- Indices de la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD PRIMARY KEY (`idJugador`),
  ADD KEY `idEquipo` (`idEquipoN`);

--
-- Indices de la tabla `medioaudiovisual`
--
ALTER TABLE `medioaudiovisual`
  ADD PRIMARY KEY (`idMedioAudiovisual`),
  ADD KEY `codigoTipoDeMedio` (`codigoTipoDeMedioN`);

--
-- Indices de la tabla `partido`
--
ALTER TABLE `partido`
  ADD PRIMARY KEY (`idPartido`),
  ADD KEY `idArbitro` (`idArbitroN`),
  ADD KEY `idEquipo1` (`idEquipoLocal`),
  ADD KEY `idEquipo2` (`idEquipoVisitante`);

--
-- Indices de la tabla `tecnico`
--
ALTER TABLE `tecnico`
  ADD PRIMARY KEY (`idTecnico`);

--
-- Indices de la tabla `tipodemedio`
--
ALTER TABLE `tipodemedio`
  ADD PRIMARY KEY (`codigoTipoDeMedio`);

--
-- Indices de la tabla `transmision`
--
ALTER TABLE `transmision`
  ADD KEY `idMedioAudiovisual` (`idMedioAudiovisualN`),
  ADD KEY `idPartido` (`idPartidoM`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `arbitro`
--
ALTER TABLE `arbitro`
  MODIFY `idArbitro` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `equipo`
--
ALTER TABLE `equipo`
  MODIFY `idEquipo` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `gol`
--
ALTER TABLE `gol`
  MODIFY `idGol` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `jugador`
--
ALTER TABLE `jugador`
  MODIFY `idJugador` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT de la tabla `medioaudiovisual`
--
ALTER TABLE `medioaudiovisual`
  MODIFY `idMedioAudiovisual` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `partido`
--
ALTER TABLE `partido`
  MODIFY `idPartido` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tecnico`
--
ALTER TABLE `tecnico`
  MODIFY `idTecnico` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tipodemedio`
--
ALTER TABLE `tipodemedio`
  MODIFY `codigoTipoDeMedio` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`idTecnicoN`) REFERENCES `tecnico` (`idTecnico`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `gol`
--
ALTER TABLE `gol`
  ADD CONSTRAINT `gol_ibfk_1` FOREIGN KEY (`idJugadorN`) REFERENCES `jugador` (`idJugador`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `gol_ibfk_2` FOREIGN KEY (`idPartidoN`) REFERENCES `partido` (`idPartido`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD CONSTRAINT `jugador_ibfk_1` FOREIGN KEY (`idEquipoN`) REFERENCES `equipo` (`idEquipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `medioaudiovisual`
--
ALTER TABLE `medioaudiovisual`
  ADD CONSTRAINT `medioaudiovisual_ibfk_1` FOREIGN KEY (`codigoTipoDeMedioN`) REFERENCES `tipodemedio` (`codigoTipoDeMedio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `partido_ibfk_1` FOREIGN KEY (`idEquipoLocal`) REFERENCES `equipo` (`idEquipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partido_ibfk_2` FOREIGN KEY (`idEquipoVisitante`) REFERENCES `equipo` (`idEquipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `partido_ibfk_3` FOREIGN KEY (`idArbitroN`) REFERENCES `arbitro` (`idArbitro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `transmision`
--
ALTER TABLE `transmision`
  ADD CONSTRAINT `transmision_ibfk_1` FOREIGN KEY (`idMedioAudiovisualN`) REFERENCES `medioaudiovisual` (`idMedioAudiovisual`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transmision_ibfk_2` FOREIGN KEY (`idPartidoM`) REFERENCES `partido` (`idPartido`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

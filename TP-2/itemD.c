#include <stdio.h>
#include <stdlib.h>

typedef struct Nodo
{
    int clave;
    struct Nodo *izquierda;
    struct Nodo *derecha;
} nodo;

void crear(nodo **);
void alta(nodo **, int);
void recorridoMayores(nodo **, int);
int contadorMayores(nodo **, int, int);


int main()
{
    nodo arbol;
    int salir, dato, opcion;

    crear(&arbol);

    while (salir != 1)
    {
        printf("\nIngrese valor para dar de alta en el arbol: \n");
        fflush(stdin);
        scanf("%d", &dato);

        alta(&arbol, dato);

        do
        {
            printf("\nIngrese opcion: \n");
            printf("0 - Nueva alta.    1 - Procesar informacion. \n");
            fflush(stdin);
            scanf("%d", &opcion);

            switch(opcion)
            {
            case 0:
                salir = 0;
                break;
            case 1:
                salir = 1;
                break;
            default:
                printf("\nOpcion Inconrrecta. \n");
            }

        }
        while(opcion != 1 && opcion != 0);
    }

    salir = 0;
    opcion = 0;

    while (salir != 1)
    {
        printf("\nIngrese valor para buscar sus mayores dentro del arbol: \n");
        fflush(stdin);
        scanf("%d", &dato);
        printf("\nMayores a %d: \n", dato);
        recorridoMayores(&arbol, dato);
        printf("\nCantidad de mayores: %d \n", contadorMayores(&arbol, dato, 0));

        do
        {
            printf("\nIngrese opcion: \n");
            printf("0 - Nueva busqueda.    1 - Salir del programa. \n");
            fflush(stdin);
            scanf("%d", &opcion);

            switch(opcion)
            {
            case 0:
                salir = 0;
                break;
            case 1:
                salir = 1;
                break;
            default:
                printf("\nOpcion Inconrrecta. \n");
            }

        }
        while(opcion != 1 && opcion != 0);
    }

    printf("\nSaliendo del programa... \n");

    return 0;
}

void crear(nodo **arbol)
{
    *arbol = NULL;
}
void alta(nodo **arbol, int dato)
{
    if (*arbol == NULL)
    {
        (*arbol) = (nodo **)malloc(sizeof(nodo));
        (*arbol)->clave = dato;
        (*arbol)->derecha = NULL;
        (*arbol)->izquierda = NULL;
        printf("\nAlta dada correctamente. \n");
    }
    else
    {
        if (dato < (*arbol)->clave)
        {
            alta(&((*arbol)->izquierda), dato);
        }
        else if (dato > (*arbol)->clave)
        {
            alta(&((*arbol)->derecha), dato);
        }
        else
        {
            printf("\nEsta clave ya existe. \n");
        }
    }
}
void recorridoMayores(nodo **arbol, int dato)
{
    if(*arbol != NULL)
    {
        recorridoMayores(&((*arbol)->izquierda), dato);
        if(dato < (*arbol)->clave)
        {
            printf("\n%d ", (*arbol)->clave);
        }
        recorridoMayores(&((*arbol)->derecha), dato);
    }

}
int contadorMayores(nodo **arbol, int dato, int cont)
{
    if(*arbol != NULL)
    {
        return contadorMayores(&((*arbol)->izquierda), dato, cont);
        if(dato < (*arbol)->clave)
        {
            cont++;
        }
        return contadorMayores(&((*arbol)->derecha), dato, cont);
    }
    return cont;
}

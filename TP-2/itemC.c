#include <stdio.h>
#include <stdlib.h>
#include<string.h>

//Estructuras de ABB
typedef struct Nodo
{
    int legajo;
    float promedio;
    char nombre[30];
    struct Nodo *izquierda;
    struct Nodo *derecha;
} nodo;
//Estructuras de Lista 1
typedef struct Nodo2
{
    int legajo;
    char nombre[30];
    struct Nodo2 * sig;
} nodo2;

typedef struct Lista1
{
    nodo2* pri;
    int cant;
} lista1;
//Estructuras de Lista 2
typedef struct Nodo3
{
    int legajo;
    int nota;
    struct Nodo3 * sig;
} nodo3;

typedef struct Lista2
{
    nodo3* pri;
    int cant;
} lista2;


//Operaciones ABB
void crearABB(nodo **);
void altaABB(nodo **, int, float, char);
void recorridoInorden(nodo **);
int estaABB(nodo **, int);
//Operaciones Lista 1
void crearLista1(lista1 *);
void altaLista1(lista1 *, int, char);
//Operaciones Lista 2
void crearLista2(lista2 *);
void altaLista2(lista2 *, int, int);

int main()
{
    lista1 list1;
    lista2 list2;
    nodo arbol;

    crearLista1(&list1);
    crearLista2(&list2);
    crearABB(&arbol);

    int dato, nota;
    char nom[30];

    do
    {
        printf("\n\nIngrese nuevo nombre para lista 1: \n");
        fflush(stdin);
        gets(nom);

        printf("\n\nIngrese legajo para lista 1 o -1 para procesar: \n");
        fflush(stdin);
        scanf("%d", &dato);

        if(dato != -1)
            altaLista1(&list1, dato, nom);
    }while(dato != -1);

    dato = 0;

    do
    {
        printf("\n\nIngrese nota para lista 2: \n", dato);
        fflush(stdin);
        scanf("%d", &nota);

        printf("\n\nIngrese legajo para lista 2 o -1 para procesar: \n");
        fflush(stdin);
        scanf("%d", &dato);

        if(dato != -1)
            altaLista2(&list2, dato, nota);
    }while(dato != -1);

    printf("\nCreando arbol del curso con datos completos... \n");
    arbolCompleto(list1, list2, &arbol);
    printf("\nMostrando arbol... \n");
    recorridoInorden(&arbol);


    return 0;
}

//Funciones ABB
void crearABB(nodo **arbol)
{
    *arbol = NULL;
}
void altaABB(nodo **arbol, int dato, float nota, char nom)
{
    if (*arbol == NULL)
    {
        (*arbol) = (nodo **)malloc(sizeof(nodo));
        (*arbol)->legajo = dato;
        (*arbol)->promedio = nota;
        strcpy(&(*arbol)->nombre, nom);
        (*arbol)->derecha = NULL;
        (*arbol)->izquierda = NULL;
        printf("\nAlta dada correctamente. \n");
    }
    else
    {
        if (dato < (*arbol)->legajo)
        {
            altaABB(&((*arbol)->izquierda), dato, nota, nom);
        }
        else if (dato > (*arbol)->legajo)
        {
            altaABB(&((*arbol)->derecha), dato, nota, nom);
        }
        else
        {
            printf("\nEsta clave ya existe. \n");
        }
    }
}
int estaABB(nodo **arbol, int dato)
{
    if (*arbol == NULL)
    {
        return 0;
    }
    else
    {
        if (dato == (*arbol)->legajo)
        {
            return 1;
        }
        else
        {
            if (dato < (*arbol)->legajo)
            {
                return estaABB(&((*arbol)->izquierda), dato);
            }
            else
            {
                return estaABB(&((*arbol)->derecha), dato);
            }
        }
    }
}
void recorridoInorden(nodo **arbol)
{
    if(*arbol != NULL)
    {
        recorridoInorden(&((*arbol)->izquierda));
        printf("\n%d ", (*arbol)->legajo);
        recorridoInorden(&((*arbol)->derecha));
    }
}
//Funciones Lista 1
void crearLista1(lista1* l)
{
    l->pri = NULL;
    l->cant = 0;
}
void altaLista1(lista1* l, int dato, char nombre)
{
    int listo = 0;

    if(l->pri == NULL)
    {
        l->pri = (nodo2*)malloc(sizeof(nodo2));
        l->pri->legajo = dato;
        strcpy(&(l->pri->nombre), nombre);
        l->pri->sig = NULL;
        l->cant += 1;
    }
    else
    {
        nodo2* actual = l->pri;

        while(actual->sig != NULL)
            {
                if(actual->legajo != dato)
                    {
                        actual = actual->sig;
                    }

                else
                    {
                        listo = 1;
                        break;
                    }
            }

        if(listo == 0)
            {
                actual->sig = (nodo2*)malloc(sizeof(nodo2));
                actual = actual->sig;
                actual->legajo = dato;
                strcpy(&(actual->nombre), nombre);
                actual->sig = NULL;
                l->cant += 1;
            }
        else
            {
                printf("\nEste legajo ya existe. \n");
            }

    }
}
//Funciones Lista 2
void crearLista2(lista2* l)
{
    l->pri = NULL;
    l->cant = 0;
}
void altaLista2(lista2* l, int dato, int nota)
{
    int listo = 0;

    if(l->pri == NULL)
    {
        l->pri = (nodo3*)malloc(sizeof(nodo3));
        l->pri->legajo = dato;
        l->pri->nota = nota;
        l->pri->sig = NULL;
        l->cant += 1;
    }
    else
    {
        nodo3* actual = l->pri;

        while(actual->sig != NULL)
            {
                actual = actual->sig;
            }

        actual->sig = (nodo3*)malloc(sizeof(nodo3));
        actual = actual->sig;
        actual->legajo = dato;
        actual->nota = nota;
        actual->sig = NULL;
        l->cant += 1;
      }
}
//Operacion adicional
void arbolCompleto(lista1 l, lista2 l2, nodo **arbol)
{
    nodo2* actual = l.pri;
    nodo3* actual2 = l2.pri;
    int cont = 0;
    float prom = 0;

    if(l.pri != NULL)
    {
        while(actual != NULL)
        {
            cont = 0;

            while(actual2 != NULL)
            {
                if(actual->legajo == actual2->legajo)
                    {
                        cont++;
                        prom += actual2->nota;
                    }
                actual2 = actual2->sig;
            }
            prom /= cont;
            altaABB(&(*arbol), actual->legajo, prom, actual->nombre);
            actual = actual->sig;
        }
    }
    else
    {
        printf("\nEsta lista se encuentra vacia.\n");
    }
}

#include <stdio.h>
#include <stdlib.h>

typedef struct Nodo
{
    int clave;
    struct Nodo *izquierda;
    struct Nodo *derecha;
} nodo;

void crear(nodo **);
void alta(nodo **, int);
void elementosComunes(nodo **, nodo **, nodo **);
void recorridoInorden(nodo **);
int esta(nodo **, int);


int main()
{
    nodo arbol;
    nodo arbol2;
    nodo arbol3;
    int salir, dato, opcion;

    crear(&arbol);
    crear(&arbol2);


    while (salir != 1)
    {
        printf("\nIngrese valor para dar de alta en el arbol 1: \n");
        fflush(stdin);
        scanf("%d", &dato);

        alta(&arbol, dato);

        do
        {
            printf("\nIngrese opcion: \n");
            printf("0 - Nueva alta en arbol 1.    1 - Cargar siguiente arbol. \n");
            fflush(stdin);
            scanf("%d", &opcion);

            switch(opcion)
            {
            case 0:
                salir = 0;
                break;
            case 1:
                salir = 1;
                break;
            default:
                printf("\nOpcion Inconrrecta. \n");
            }

        }
        while(opcion != 1 && opcion != 0);
    }

    salir = 0;
    opcion = 0;

    while (salir != 1)
    {
        printf("\nIngrese valor para dar de alta en el arbol 2: \n");
        fflush(stdin);
        scanf("%d", &dato);

        alta(&arbol2, dato);

        do
        {
            printf("\nIngrese opcion: \n");
            printf("0 - Nueva alta en arbol.    1 - Crear nuevo arbol con elementos comunes de los anteriores. \n");
            fflush(stdin);
            scanf("%d", &opcion);

            switch(opcion)
            {
            case 0:
                salir = 0;
                break;
            case 1:
                salir = 1;
                break;
            default:
                printf("\nOpcion Inconrrecta. \n");
            }

        }
        while(opcion != 1 && opcion != 0);
    }

    printf("\n\nCreando nuevo arbol con elementos comunes...\n");
    crear(&arbol3);
    elementosComunes(&arbol, &arbol2, &arbol3);
    printf("\n\nMostrando arbol inorden: \n");
    recorridoInorden(&arbol3);

    printf("\n\nSaliendo del programa... \n");

    return 0;
}

void crear(nodo **arbol)
{
    *arbol = NULL;
}
void alta(nodo **arbol, int dato)
{
    if (*arbol == NULL)
    {
        (*arbol) = (nodo **)malloc(sizeof(nodo));
        (*arbol)->clave = dato;
        (*arbol)->derecha = NULL;
        (*arbol)->izquierda = NULL;
    }
    else
    {
        if (dato < (*arbol)->clave)
        {
            alta(&((*arbol)->izquierda), dato);
        }
        else if (dato > (*arbol)->clave)
        {
            alta(&((*arbol)->derecha), dato);
        }
        else
        {
            printf("\nEsta clave ya existe. \n");
        }
    }
}
void elementosComunes(nodo **arbol, nodo **arbol2, nodo **arbol3)
{
    if(*arbol != NULL)
    {
        elementosComunes(&((*arbol)->izquierda), &(*arbol2), &(*arbol3));

        if(esta(&(*arbol2), (*arbol)->clave) == 1)
        {
            alta(&(*arbol3), (*arbol)->clave);
        }

        elementosComunes(&((*arbol)->derecha), &(*arbol2), &(*arbol3));
    }
}
int esta(nodo **arbol, int dato)
{
    if (*arbol == NULL)
    {
        return 0;
    }
    else
    {
        if (dato == (*arbol)->clave)
        {
            return 1;
        }
        else
        {
            if (dato < (*arbol)->clave)
            {
                return esta(&((*arbol)->izquierda), dato);
            }
            else
            {
                return esta(&((*arbol)->derecha), dato);
            }
        }
    }
}
void recorridoInorden(nodo **arbol)
{
    if(*arbol != NULL)
    {
        recorridoInorden(&((*arbol)->izquierda));
        printf("\n%d ", (*arbol)->clave);
        recorridoInorden(&((*arbol)->derecha));
    }
}

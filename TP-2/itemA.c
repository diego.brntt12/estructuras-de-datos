#include <stdio.h>
#include <stdlib.h>

typedef struct Nodo
{
    int clave;
    struct Nodo *izquierda;
    struct Nodo *derecha;
} nodo;
typedef struct Nodo2
{
    int clave;
    int repeticiones;
    struct Nodo2 *izquierda;
    struct Nodo2 *derecha;
} nodo2;

void crear(nodo **);
void crear2(nodo2 **);
void alta(nodo **, int);
void altaSin(nodo2 **, int);
void contadorRepes(nodo **, nodo2 **);
void recorridoInorden(nodo2 **);

int main()
{
    nodo arbol;
    nodo2 arbol2;
    int salir, dato, opcion;

    crear(&arbol);
    crear2(&arbol2);

    while (salir != 1)
    {
        printf("\nIngrese valor para dar de alta en el arbol (permite repeticiones): \n");
        fflush(stdin);
        scanf("%d", &dato);

        alta(&arbol, dato);

        do
        {
            printf("\nIngrese opcion: \n");
            printf("0 - Nueva alta.    1 - Generar nuevo arbol con contador de repeticiones. \n");
            fflush(stdin);
            scanf("%d", &opcion);

            switch(opcion)
            {
            case 0:
                salir = 0;
                break;
            case 1:
                salir = 1;
                break;
            default:
                printf("\nOpcion Inconrrecta. \n");
            }

        }
        while(opcion != 1 && opcion != 0);
    }

    printf("\n\nCreando arbol con contador de repeticiones... \n");
    contadorRepes(&arbol, &arbol2);
    printf("\nMostrando arbol inorden: \n");
    recorridoInorden(&arbol2);

    return 0;
}

void crear(nodo **arbol)
{
    *arbol = NULL;
}
void crear2(nodo2 **arbol)
{
    *arbol = NULL;
}
void alta(nodo **arbol, int dato)
{
    if (*arbol == NULL)
    {
        (*arbol) = (nodo **)malloc(sizeof(nodo));
        (*arbol)->clave = dato;
        (*arbol)->derecha = NULL;
        (*arbol)->izquierda = NULL;
        printf("\nAlta dada correctamente. \n");
    }
    else
    {
        if (dato < (*arbol)->clave)
        {
            alta(&((*arbol)->izquierda), dato);
        }
        else if (dato >= (*arbol)->clave)
        {
            alta(&((*arbol)->derecha), dato);
        }
    }
}
void altaSin(nodo2 **arbol, int dato)
{
    if (*arbol == NULL)
    {
        (*arbol) = (nodo2 **)malloc(sizeof(nodo2));
        (*arbol)->clave = dato;
        (*arbol)->repeticiones = 1;
        (*arbol)->derecha = NULL;
        (*arbol)->izquierda = NULL;
    }
    else
    {
        if (dato < (*arbol)->clave)
        {
            altaSin(&((*arbol)->izquierda), dato);
        }
        else if (dato > (*arbol)->clave)
        {
            altaSin(&((*arbol)->derecha), dato);
        }
        else
        {
            (*arbol)->repeticiones += 1;
        }
    }
}
void contadorRepes(nodo **arbol, nodo2 **arbol2)
{
    if(*arbol != NULL)
    {
        contadorRepes(&((*arbol)->izquierda), &(*arbol2));
        altaSin(&(*arbol2), (*arbol)->clave);
        contadorRepes(&((*arbol)->derecha), &(*arbol2));
    }
}
void recorridoInorden(nodo2 **arbol)
{
    if(*arbol != NULL)
    {
        recorridoInorden(&((*arbol)->izquierda));
        printf("\nClave: %d \nRepeticiones: %d \n", (*arbol)->clave, (*arbol)->repeticiones);
        recorridoInorden(&((*arbol)->derecha));
    }
}

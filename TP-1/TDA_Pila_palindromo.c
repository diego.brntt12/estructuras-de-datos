#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define TAM 30

typedef struct Nodo
{
    char car;
    struct Nodo *sig;
} nodo;

typedef struct Pila
{
    nodo *primero;
    int cant_elementos;
    char tope;
} pila;

void crear(pila *);
void destruir(pila *);
void recorrer(pila);
void apilar(pila *, char);
void desapilar(pila *);
int esPalindromo(pila);

int main()
{
    int seguir = 0, salir = 0;
    char palabra[TAM], opc;
    pila pal;

    printf("----------------------------------------------\n");
    printf("| BIENVENIDO/A AL VERIFICADOR DE PALINDROMOS |\n");
    printf("----------------------------------------------\n");

    while (seguir != 1)
    {
        crear(&pal);

        printf("\nIngrese la palabra que desea verificar: \n");
        fflush(stdin);
        scanf("%s", &palabra);

        for (int i = 0; i < (strlen(palabra)); i++)
        {
            apilar(&pal, palabra[i]);
        }

        recorrer(pal);

        if (esPalindromo(pal) == 1)
        {
            printf("\n%s es Palindromo. \n", palabra);
        }
        else
        {
            printf("\n%s no es Palindromo. \n", palabra);
        }
    }

    return 0;
}

void crear(pila *pal)
{
    pal->primero = NULL;
    pil->tope = -1;
    pal->cant_elementos = 0;
}
void apilar(pila *pal, char dato)
{
    nodo *nuevo = (nodo *)malloc(sizeof(nodo));
    nuevo->car = dato;

    if (pal->primero == NULL)
    {
        nuevo->sig = pal->primero;
        pal->primero = nuevo;
        pal->tope = dato;
    }
    else
    {
        nuevo->sig = pal->primero;
        pal->primero = nuevo;
        pal->tope = dato;
    }
    pal->cant_elementos++;
}
void desapilar(pila *pil)
{
    nodo *actual = pil->primero;
    nodo *anterior = (nodo *)malloc(sizeof(nodo));
    anterior = NULL;

    if (pil->primero != NULL)
    {
        pil->primero = pil->primero->sig;
        if (pil->primero == NULL)
            pil->tope = -1;
        else
            pil->tope = pil->primero->car;

        anterior = actual;
        actual = actual->sig;

        free(anterior);
    }
    else
    {
        printf("\nPila inexistente.\n");
    }
}
void recorrer(pila pal)
{
    nodo *actual = pal.primero;

    if (pal.primero != NULL)
    {
        while (actual != NULL)
        {
            printf("\n%c\n", actual->car);
            actual = actual->sig;
        }
    }
    else
    {
        printf("\nEsta pila se encuentra vacia.\n");
    }
}
void destruir(pila *pil)
{
    nodo *actual = pil->primero;

    if (pil->primero != NULL)
    {

        while (pil->primero != NULL)
        {
            actual = pil->primero;
            pil->primero = pil->primero->sig;
            pil->tope = -1;
            pil->cant_elementos = 0;
            free(actual);
        }
    }
    else
    {
        printf("\nPila inexistente.\n");
    }
}
int esPalindromo(pila pal)
{
    int longitud = pal.cant_elementos / 2;
    pila aux;
    int cont = 0;

    crear(&aux);

    if ((pal.cant_elementos % 2) == 0)
    {

        for (int i = 0; i < longitud; i++)
        {
            apilar(&aux, pal.tope);
            desapilar(&pal);
            cont++;
        }

        while (pal.tope == aux.tope)
        {
            if (cont <= 1)
            {
                destruir(&aux);
                return 1;
            }
            else
            {
                desapilar(&pal);
                desapilar(&aux);
                cont--;
            }
        }
        destruir(&aux);
        return 0;
    }
    else
    {
        for (int i = 0; i < longitud; i++)
        {
            apilar(&aux, pal.tope);
            desapilar(&pal);
            cont++;
        }

        desapilar(&pal);

        while (pal.tope == aux.tope)
        {
            if (cont <= 1)
            {
                destruir(&aux);
                return 1;
            }
            else
            {
                desapilar(&pal);
                desapilar(&aux);
                cont--;
            }
        }
        destruir(&aux);
        return 0;
    }
}
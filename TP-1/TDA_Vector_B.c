#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Alumno
{
    int legajo;
    int nota;
    char materia[20];
} alumno;

typedef struct Vector
{
    alumno *alum;
    int tam;
} vector;

void crear(vector *, int);
void carga(vector *);
void cambiar(vector *, int, alumno);
void aprobados(vector);
void ordenarLegajo(vector);
void destruir(vector *);

int main()
{
    vector vec;
    int cant_alumnos;

    printf("\nIngrese cantidad de alumnos del curso: \n");
    scanf("%d", &cant_alumnos);

    crear(&vec, cant_alumnos);

    carga(&vec);

    ordenarLegajo(vec);

    aprobados(vec);

    destruir(&vec);

    return 0;
}

void crear(vector *vec, int dato)
{
    vec->tam = dato;
    vec->alum = (alumno *)malloc(sizeof(alumno) * dato);
    printf("\nVector del curso creado de manera satisfactoria.\n");
}
void cambiar(vector *vec, int pos, alumno dato)
{
    vec->alum[pos] = dato;
}
void carga(vector *vec)
{
    alumno aux;

    for (int i = 0; i < vec->tam; i++)
    {
        printf("\nIngrese numero de legajo: \n");
        scanf("%d", &aux.legajo);

        printf("\nIngrese nombre de la materia: \n");
        scanf("%s", &aux.materia);

        printf("\nIngrese numero de nota: \n");
        scanf("%d", &aux.nota);

        cambiar(vec, i, aux);
    }
}
void aprobados(vector vec)
{
    int legajo_aux, cont2 = 0;

    for (int i = 0, j = 0, cont1 = 0; i < vec.tam; i = j)
    {
        legajo_aux = vec.alum[i].legajo;
        while (legajo_aux == vec.alum[j].legajo)
        {
            if (vec.alum[j].nota >= 6) // aprobado
                cont1++;
            j++;
        }
        if (cont1 > 3)
            cont2 += 1;
        cont1 = 0;
    }

    printf("\nCantidad de alumnos que aprobaron 4 o mas materias: %d\n", cont2);
}
void ordenarLegajo(vector vec)
{
    alumno aux;

    // "Burbuja" auxiliar para mandar el vector ordenado por legajo para analizar.
    for (int i = 0; i < vec.tam - 1; i++)
    {
        for (int j = 0; j < vec.tam - i - 1; j++)
        {

            if (vec.alum[j].legajo > vec.alum[j + 1].legajo)
            {

                aux.legajo = vec.alum[j].legajo;
                strcpy(aux.materia, vec.alum[j].materia);
                aux.nota = vec.alum[j].nota;

                vec.alum[j].legajo = vec.alum[j + 1].legajo;
                strcpy(vec.alum[j].materia, vec.alum[j + 1].materia);
                vec.alum[j].nota = vec.alum[j + 1].nota;

                vec.alum[j + 1].legajo = aux.legajo;
                strcpy(vec.alum[j + 1].materia, aux.materia);
                vec.alum[j + 1].nota = aux.nota;
            }
        }
    }
}
void destruir(vector *vec)
{
    if (vec->alum != NULL)
    {
        free(vec->alum);
    }
}
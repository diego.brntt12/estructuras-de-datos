#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct Dato
{
    int ticket;
    int posicion;
} dato;

typedef struct Cola
{
    dato *dat;
    int tam;
    int frente;
    int fondo;
} cola;

void crear(cola *, int);
void acolar(cola *, dato, int);
void desacolar(cola *);
void cargar(cola *);
void recorrer(cola);
int vacia(cola);
void inspectorDeTickets(cola);

int main()
{
    cola col;
    int tam;

    printf("\nIngrese tamaño deseado de la cola: \n");
    scanf("%d", &tam);

    crear(&col, tam);

    if (vacia(col) == 0)
        printf("\nCola Vacia.\n");
    else
        printf("\nCola con elementos.\n");

    cargar(&col);
    printf("\nCola completa.\n");
    recorrer(col);
    inspectorDeTickets(col);

    return 0;
}

void crear(cola *col, int ta)
{
    col->dat = (dato *)malloc(sizeof(dato) * ta);
    col->tam = ta;
    col->frente = -1;
    col->fondo = -1;
    printf("\nCola con %d elementos creada.\n", col->tam);
}
void acolar(cola *col, dato da, int i)
{

    if (col->fondo == (col->tam - 1))
    {
        printf("\nCola llena.\n");
    }
    else
    {
        col->dat[i] = da;

        if (col->fondo == col->frente && col->frente == -1)
        {
            col->frente = i;
            col->fondo = i;
        }
        else
        {
            col->fondo = i;
        }
    }
}
void cargar(cola *col)
{
    dato aux;
    for (int i = 0; i < col->tam; i++)
    {
        printf("\nIngrese numero de atencion para la persona %d: \n", i + 1);
        scanf("%d", &aux.posicion);

        do
        {
            printf("\nTiene ticket? \n 0-no 1-si \n");
            fflush(stdin);
            scanf("%d", &aux.ticket);
        } while (aux.ticket < 0 || aux.ticket > 1);

        acolar(col, aux, i);
    }
}
void recorrer(cola col)
{
    if (vacia(col) == 0)
    {
        printf("\nCola Vacia.\n");
    }
    else
    {
        for (int i = col.frente; i <= col.fondo; i++)
        {
            printf("Numero de atencion: %d \n", col.dat[i].posicion);
        }
    }
}
void desacolar(cola *col)
{
    if (col->fondo == col->frente && col->frente != -1)
    {
        col->frente = -1;
        col->fondo = col->frente;
    }
    else
    {
        if (col->frente <= col->fondo)
        {
            col->frente++;
        }
        else
        {
            col->frente = -1;
            col->fondo = col->frente;
        }
    }
}
int vacia(cola col)
{
    if (col.frente == col.fondo && col.frente == -1)
        return 0;
    else
        return -1;
}
void inspectorDeTickets(cola col)
{
    int cont = 0;
    dato aux;
    cola aux2;

    if (vacia(col) == 0)
    {
        printf("\nCola Vacia.\n");
    }
    else
    {
        printf("\nColados: \n");

        crear(&aux2, col.tam);

        for (int i = col.frente; i <= col.fondo; i++)
        {
            aux = col.dat[i];
            if (aux.ticket == 0)
            {
                printf("Numero de atencion: %d\tno tiene ticket. \n", col.dat[i].posicion);
                desacolar(&col);
            }
            else
            {
                desacolar(&col);
                acolar(&aux2, aux, aux2.fondo+1);
            }
        }

        printf("\nCola sin colados: \n");
        recorrer(aux2);
    }
}